const path = require('path');

module.exports = {
  title: 'Hosco Styleguide',
  ignore: ['**/__tests__/**', '**/components/*/index.js'],
  pagePerSection: true,
  styleguideComponents: {
    Wrapper$: path.join(__dirname, 'src/styleguide/components/Wrapper'),
    Playground$: path.join(__dirname, 'src/styleguide/components/Playground'),
  },
  context: {
    noop: 'lodash-es/noop',
  },
  require: [
    path.join(__dirname, 'src/styleguide/style.css')
  ],
  getExampleFilename(componentPath) {
    const dirname = path.dirname(componentPath);
    const filename = path.basename(componentPath, '.js');

    return path.resolve(dirname, 'docs', filename + '.md');
  },
  webpackConfig: require('react-scripts/config/webpack.config'),
  template: {
    head: {
      links: [
        {
          href: 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|Montserrat:400,700',
          rel: 'stylesheet',
        }
      ]
    }
  },
  theme: {
    fontFamily: {
      base: '"Open Sans", sans-serif'
    },
    color: {
      link: '#1898c2',
      linkHover: '#115c87'
    },
  },
  styles: {
    ComponentsList: {
      item: {
        fontSize: '13px'
      }
    }
  },
  sections: [
    {
      name: 'Guidelines',
      content: 'src/styleguide/pages/guidelines.md'
    },
    {
      name: 'Components',
      sectionDepth: 2,
      sections: ['UI', 'Form', 'Layout', 'News', 'Widget'].map(name => ({
        name: name,
        components: `src/components/${ name }/*.js`,
        sectionDepth: 2,
      }))
    },
  ]
};
