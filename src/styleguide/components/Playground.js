import React, { useState } from 'react';
import styled from '@emotion/styled';
import Playground from 'react-styleguidist/lib/rsg-components/Playground/Playground';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';

const Toggle = styled(FontAwesomeIcon)`
  cursor: pointer;
  position: absolute;
  z-index: 2;
  bottom: 10px;
  right: 40px;
  color: #767676;
`;

const Container = styled('div')`
  position: relative;
`;

export default ({ settings, ...props }) => {
  const [transparent, setTransparent] = useState(false);

  const playgroundSettings = { props: {}, ...settings};

  let classNames = [
    transparent && 'preview-transparent',
    settings.dark && 'is-dark',
    settings.sidebar && 'is-sidebar',
    settings.content && 'is-content',
  ];

  playgroundSettings.props.className = classNames.filter(Boolean).join(' ');

  return (
    <Container>
      { !settings.noeditor &&
          <Toggle onClick={() => setTransparent(!transparent)} icon={ transparent ? faEyeSlash : faEye } />
      }
      <Playground { ...props } settings={ playgroundSettings } />
    </Container>
  );
}
