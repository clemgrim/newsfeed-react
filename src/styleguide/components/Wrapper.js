import React  from 'react'
import { MemoryRouter } from 'react-router-dom';
import { ThemeProvider } from 'emotion-theming';
import Wrapper from 'react-styleguidist/lib/rsg-components/Wrapper/Wrapper';

import theme from '../../theme';

export default ({ children, ...props }) => (
  <ThemeProvider theme={ theme }>
    <MemoryRouter>
      <Wrapper { ...props }>{ children }</Wrapper>
    </MemoryRouter>
  </ThemeProvider>
);
