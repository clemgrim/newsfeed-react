import React, { useRef } from 'react';
import styled from '@emotion/styled';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCopy } from '@fortawesome/free-solid-svg-icons';

import theme from '../../theme';

const Container = styled('div')`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 30px;
`;

const Palette = styled('div')`
  background: ${ props => props.color };
  height: 60px;
  margin-bottom: 5px;
`;

const Info = styled('div')`
  display: flex;
  justify-content: space-between;
`;

const CopyIcon = styled(FontAwesomeIcon)`
  cursor: pointer;
  margin-left: 5px;

  &:hover {
    color: #666;
  }
`;

const PaletteContainer = ({ color, name }) => {
  const colorRef = useRef(null);

  const copyColor = () => {
    const range = document.createRange();
    const selection = document.getSelection();

    range.selectNodeContents(colorRef.current);
    selection.removeAllRanges();
    selection.addRange(range);
    document.execCommand('copy');
  };

  return (
    <div>
      <Palette color={ color } />
      <Info>
        <strong>{ name }</strong>
        <div>
          <span ref={ colorRef }>{ color.toLowerCase() }</span>
          <CopyIcon icon={ faCopy } onClick={copyColor} />
        </div>
      </Info>
    </div>
  );
};

export default () => {
  return (
    <Container>
      { Object.entries(theme.colors).map(([name, color ]) => <PaletteContainer key={ name }  name={ name } color={ color } />) }
    </Container>
  );
};
