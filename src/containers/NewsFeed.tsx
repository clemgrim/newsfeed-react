import { connect } from 'react-redux';

import NewsFeed from '../components/News/NewsFeed';
import { fetchNewsfeed, shareNews, NewsfeedActions } from '../store/newsfeed';
import { AppState } from '../store';

const mapStateToProps = ({
  newsfeed: { isLoading, items, error }
}: AppState) => ({
  isLoading,
  items,
  error
});

const mapDispatchToProps = {
  loadNews: fetchNewsfeed,
  onLikeToggled: NewsfeedActions.likeToggled,
  onShared: shareNews
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewsFeed);
