import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import NewsItem from '../components/News/NewsItem';
import { fetchNews } from '../store/news';
import { AppState } from '../store';
import { News } from '../store/models';

class NewsDetails extends PureComponent<NewsDetailsProps> {
  componentDidMount() {
    this.props.loadNews(this.props.newsId);
  }

  render() {
    const { news, ...otherProps } = this.props;
    return news ? (
      <NewsItem
        news={news}
        onLikeToggled={() => null}
        onShared={() => null}
        {...otherProps}
      />
    ) : (
      'Loading...'
    );
  }
}

const mapStateToProps = ({ news }: AppState) => ({
  news: news.item
});

const mapDispatchToProps = {
  loadNews: fetchNews
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewsDetails);

interface NewsDetailsProps {
  news?: News;
  loadNews: Function;
  newsId: number;
}
