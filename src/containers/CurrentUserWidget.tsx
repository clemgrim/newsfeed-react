import { connect } from 'react-redux';

import ProfileWidget from '../components/Widget/ProfileWidget';
import { AppState } from '../store';

const mapStateToProps = ({ identity }: AppState) => {
  const user = identity || ({} as { [k: string]: any });

  return {
    name: user.name,
    info: user.title,
    avatar: user.avatar,
    link: user.url
  };
};

export default connect(mapStateToProps)(ProfileWidget);
