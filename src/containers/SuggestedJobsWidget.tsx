import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { faSuitcase } from '@fortawesome/free-solid-svg-icons';

import Widget from '../components/Widget/WidgetBase';
import WidgetListItem from '../components/Widget/ListItem';
import { fetchSuggestedJobs } from '../store/suggestedJobs';
import { AppState } from '../store';
import { SuggestedJob } from '../store/models';

class SuggestedJobsWidget extends PureComponent<SuggestedJobsWidgetProps> {
  componentDidMount() {
    this.props.loadJobs();
  }

  render() {
    const { suggestedJobs, ...otherProps } = this.props;
    return (
      <Widget title="Suggested jobs" icon={faSuitcase} {...otherProps}>
        {this.props.suggestedJobs.map(job => (
          <WidgetListItem key={job.id} {...job} />
        ))}
      </Widget>
    );
  }
}

const mapStateToProps = ({ suggestedJobs }: AppState) => ({
  suggestedJobs: suggestedJobs.items
});

const mapDispatchToProps = {
  loadJobs: fetchSuggestedJobs
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SuggestedJobsWidget);

interface SuggestedJobsWidgetProps {
  loadJobs: Function;
  suggestedJobs: SuggestedJob[];
}
