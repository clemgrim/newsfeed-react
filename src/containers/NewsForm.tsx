import { connect } from 'react-redux';

import NewsForm from '../components/News/NewsForm';
import { createNews } from '../store/newsfeed';

const mapDispatchToProps = {
  onSubmit: createNews
};

export default connect(
  null,
  mapDispatchToProps
)(NewsForm);
