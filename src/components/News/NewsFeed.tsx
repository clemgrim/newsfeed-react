import React, { PureComponent } from 'react';

import styled from '../../theme';
import NewsItem from './NewsItem';
import { News } from '../../store/models';

const StyledNews = styled(NewsItem)`
  & + & {
    margin-top: 20px;
  }
`;

class NewsFeed extends PureComponent<NewsFeedProps> {
  componentDidMount() {
    this.props.loadNews();
  }

  render() {
    const { items, isLoading, error, onLikeToggled, onShared } = this.props;

    if (error) {
      return 'An error occurred';
    }

    if (isLoading) {
      return 'Loading...';
    }

    return items.map(item => (
      <StyledNews key={item.id} news={item} {...{ onLikeToggled, onShared }} />
    ));
  }
}

export default NewsFeed;

interface NewsFeedProps {
  items: News[];
  loadNews: Function;
  isLoading?: boolean;
  error?: Error;
  onLikeToggled: (id: number) => any;
  onShared: (id: number) => any;
}
