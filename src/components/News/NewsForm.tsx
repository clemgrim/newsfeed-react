import React, { useState, FunctionComponent } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFile, faCamera } from '@fortawesome/free-solid-svg-icons';

import { Avatar, Button } from '../UI';
import styled from '../../theme';

const Container = styled('div')`
  background-color: #fff;
  padding: 10px 15px;
  margin-bottom: 20px;
  border-bottom: 1px solid #ccc;
  border-right: 1px solid #d5d5d5;
`;

const Messenger = styled('div')`
  display: flex;
  margin-bottom: 10px;
`;

const InputContainer = styled('div')`
  padding-left: 15px;
  position: relative;
  flex: 1 1 0;
  min-width: 1%;
`;

const Input = styled('textarea')<{ expanded: boolean }>`
  font-size: 0.875rem;
  transition: border 0.25s ease-in;
  border: 0;
  border-bottom: 1px solid #e4e4e4;
  background-color: #fff;
  padding: 10px 10px 10px 0;
  overflow: visible;
  position: relative;
  min-width: 1%;
  z-index: 2;
  width: 100%;
  height: ${props => (props.expanded ? 'auto' : '45px')};
  word-wrap: break-word;
  white-space: pre-line;
  outline: none;
`;

const SubmitButton = styled(Button)`
  margin-left: auto;
`;

const Actions = styled('div')`
  display: flex;
  width: 100%;
`;

const ActionIcon = styled(FontAwesomeIcon)`
  color: ${props => props.theme.colors.primary};
  cursor: pointer;
`;

const NewsForm: FunctionComponent<NewsFormProps> = ({
  avatar,
  placeholder = 'Write something...',
  allowPdf = false,
  onSubmit
}) => {
  const [expanded, setExpanded] = useState(false);
  const [message, setMessage] = useState('');
  const onSubmitHandler = () => {
    onSubmit(message);
    setMessage('');
  };

  return (
    <Container>
      <Messenger>
        {avatar && <Avatar src={avatar} />}
        <InputContainer>
          <Input
            value={message}
            placeholder={placeholder}
            expanded={expanded}
            onBlur={() => setExpanded(!!message)}
            onFocus={() => setExpanded(true)}
            onChange={e => setMessage(e.target.value)}
          />
        </InputContainer>
      </Messenger>
      <Actions>
        <div>
          <ActionIcon icon={faCamera} />
          {allowPdf && <ActionIcon icon={faFile} />}
        </div>
        <SubmitButton
          onClick={() => onSubmitHandler()}
          theming="primary"
          size="xs"
        >
          Publish
        </SubmitButton>
      </Actions>
    </Container>
  );
};

export default NewsForm;

interface NewsFormProps {
  avatar: string;
  placeholder?: string;
  allowPdf?: boolean;
  onSubmit: Function;
}
