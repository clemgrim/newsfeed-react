import React, { FunctionComponent } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart, faShareAlt } from '@fortawesome/free-solid-svg-icons';
import {
  faHeart as faHeartRegular,
  faComments
} from '@fortawesome/free-regular-svg-icons';
import { Avatar, DateTime } from '../UI';
import styled from '../../theme';
import { News } from '../../store/models';

const Container = styled('div')`
  background-color: #fff;
  position: relative;
  display: block;
  border-bottom: 1px solid #ccc;
  border-right: 1px solid #d5d5d5;
  padding: 20px 15px 0;
  color: #666;
  font-size: 13px;
`;

const Header = styled('div')`
  display: flex;
  font-weight: bold;
  font-size: 13px;
  margin-bottom: 20px;
`;

const Info = styled('div')`
  flex: 1 1 0;
  display: flex;
  flex-wrap: wrap;
`;

const ProfileAvatar = styled(Avatar)`
  margin-right: 10px;
`;

const ProfileName = styled('div')`
  color: ${props => props.theme.colors.secondary};
`;

const Story = styled('div')`
  color: #999;
  margin-left: 5px;
`;

const Date = styled(Link)`
  flex: 0 0 100%;
  font-weight: normal;
  font-size: 11px;
  margin-top: auto;
  color: inherit;
  text-decoration: none;
`;

const Content = styled('div')`
  word-wrap: break-word;
`;

const AttachmentContainer = styled('div')`
  margin-top: 10px;
`;

const AttachmentPicture = styled('img')`
  max-width: 100%;
  height: auto;
`;

const Comments = styled('div')`
  margin: 0 -15px;
`;

const SocialContainer = styled('div')`
  display: flex;
  justify-content: space-between;
  padding: 15px 0;
  font-size: 13px;
`;

const SocialAction = styled('span')`
  margin-right: 10px;
  cursor: pointer;

  &:hover {
    color: ${props => props.theme.colors.primary};
  }
`;

const Counter = styled('span')`
  margin-left: 10px;
`;

const NewsItem: FunctionComponent<NewsItemProps> = ({
  news,
  onLikeToggled,
  onShared,
  ...otherProps
}) => {
  return (
    <Container {...otherProps}>
      <Header>
        <ProfileAvatar src={news.owner.avatar} title={news.owner.name} />
        <Info>
          <ProfileName>{news.owner.name}</ProfileName>
          <Story>{news.story}</Story>
          <Date to={`/news/${news.id}`}>
            <DateTime date={news.creation_date} />
          </Date>
        </Info>
      </Header>
      <Content>{news.content}</Content>
      <AttachmentContainer>
        {news.type === 'picture' &&
          news.thumbs.map(({ path, title }) => (
            <AttachmentPicture key={path} src={path} alt={title} />
          ))}
      </AttachmentContainer>
      <SocialContainer>
        <div>
          {news.is_liked ? (
            <SocialAction onClick={() => onLikeToggled(news.id)}>
              <FontAwesomeIcon icon={faHeart} /> Unlike
            </SocialAction>
          ) : (
            <SocialAction onClick={() => onLikeToggled(news.id)}>
              <FontAwesomeIcon icon={faHeartRegular} /> Like
            </SocialAction>
          )}

          <SocialAction>
            <FontAwesomeIcon icon={faComments} /> Comment
          </SocialAction>
          <SocialAction onClick={() => onShared(news.id)}>
            <FontAwesomeIcon icon={faShareAlt} /> Share
          </SocialAction>
        </div>
        <div>
          {news.like_count > 0 && (
            <Counter>
              {news.like_count} <FontAwesomeIcon icon={faHeart} />
            </Counter>
          )}
          {news.comment_count > 0 && (
            <Counter>
              {news.comment_count} <FontAwesomeIcon icon={faComments} />
            </Counter>
          )}
          {news.share_count > 0 && (
            <Counter>
              {news.share_count} <FontAwesomeIcon icon={faShareAlt} />
            </Counter>
          )}
        </div>
      </SocialContainer>
      <Comments />
    </Container>
  );
};

export default NewsItem;

interface NewsItemProps {
  news: News;
  onLikeToggled: (id: number) => any;
  onShared: (id: number) => any;
}
