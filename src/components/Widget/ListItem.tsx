import React, { FunctionComponent } from 'react';
import { css } from '@emotion/core';
import styled from '../../theme';
import Avatar from '../UI/Avatar';
import { string } from 'prop-types';

const textTruncate = css`
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`;

const WidgetListItemContainer = styled('div')`
  display: flex;
  align-items: center;
  width: 100%;
  height: 40px;
  position: relative;

  &:not(:last-child) {
    margin-bottom: 10px;
  }
`;

const WidgetListItemTitle = styled('div')`
  ${textTruncate};
  flex: 1 1 0;
  padding: 0 0 0 10px;
`;

const WidgetListItemName = styled('a')`
  font-size: 13px;
  color: #1898c2;
  display: inline;
  text-decoration: none;
`;

const WidgetListItemInfo = styled('div')`
  ${textTruncate};
  font-size: 12px;
  display: block;
  width: auto;
`;

const WidgetListItem: FunctionComponent<WidgetListItemProps> = ({
  title,
  info,
  link,
  picture,
  ...otherProps
}) => {
  return (
    <WidgetListItemContainer {...otherProps}>
      <Avatar size={'xs'} link={link} title={title} src={picture} />
      <WidgetListItemTitle>
        <WidgetListItemName href={link}>{title}</WidgetListItemName>
        <WidgetListItemInfo>{info}</WidgetListItemInfo>
      </WidgetListItemTitle>
    </WidgetListItemContainer>
  );
};

export default WidgetListItem;

interface WidgetListItemProps {
  title: string;
  info: string;
  link: string;
  picture: string;
}
