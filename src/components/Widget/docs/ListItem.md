```jsx
<WidgetListItem title="My title" link="#" info="Lorem ipsum dolor sit amet" picture="https://placehold.it/40x40" />
```

### Inside a widget
```jsx dark sidebar
const Widget = require('../WidgetBase').default;

<Widget title="My title">
    <WidgetListItem
      title="My title"
      link="#"
      info="Lorem ipsum dolor sit amet"
      picture="https://placehold.it/40x40"
    />
    <WidgetListItem
      title="My title II"
      link="#"
      info="Lorem ipsum dolor sit amet"
      picture="https://placehold.it/40x40"
    />
    <WidgetListItem
      title="My title lorem ipsum dolor sit amet lorem ipsum"
      link="#"
      info="Lorem ipsum dolor sit amet lorem ipsum dolor"
      picture="https://placehold.it/40x40"
    />
</Widget>
```
