```jsx dark sidebar
const faCogs = require('@fortawesome/free-solid-svg-icons').faCogs;

<WidgetBase title="My title" icon={ faCogs }>This is my widget</WidgetBase>
```

### Without icon
```jsx dark sidebar
<WidgetBase title="My title">This is my widget</WidgetBase>
```

### Without title
```jsx dark sidebar
<WidgetBase>This is my widget</WidgetBase>
```

### Multiple widgets
```jsx dark sidebar
<WidgetBase title="Welcome">Widget I</WidgetBase>
<WidgetBase>Widget II</WidgetBase>
<WidgetBase>Widget III</WidgetBase>
```
