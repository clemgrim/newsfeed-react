import React, { FunctionComponent } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconProp } from '@fortawesome/fontawesome-svg-core';

import styled from '../../theme';

const Widget = styled('div')`
  white-space: normal;
  background-color: #fff;
  position: relative;
  display: block;
  border-bottom: 1px solid #ccc;
  border-right: 1px solid #d5d5d5;
  padding: 10px;
  width: 100%;

  &:not(:last-child) {
    margin-bottom: 20px;
  }
`;

const WidgetTitle = styled('div')`
  font-size: 0.8125rem;
  text-transform: uppercase;
  padding-bottom: 10px;
  margin: 0 0 10px;
  border-bottom: 1px solid #eee;
`;

const WidgetIcon = styled(FontAwesomeIcon)`
  color: #1898c2;
  margin-right: 5px;
  font-size: 0.9375rem;
`;

const WidgetBase: FunctionComponent<WidgetProps> = ({
  title,
  icon,
  children,
  ...otherProps
}) => {
  return (
    <Widget {...otherProps}>
      {title ? (
        <WidgetTitle>
          {icon && <WidgetIcon icon={icon} />} {title}
        </WidgetTitle>
      ) : (
        ''
      )}

      {children}
    </Widget>
  );
};

export default WidgetBase;

interface WidgetProps {
  title?: string;
  icon?: IconProp;
}
