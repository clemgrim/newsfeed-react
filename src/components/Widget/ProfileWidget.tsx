import React, { FunctionComponent } from 'react';
import styled from '../../theme';
import { Avatar } from '../UI';
import WidgetBase from './WidgetBase';

const ProfileWidgetContainer = styled(WidgetBase)`
  display: flex;
`;

const ProfileWidgetAvatar = styled(Avatar)`
  flex: 0 0 40px;
  margin-right: 10px;
`;

const ProfileWidgetInfo = styled('div')`
  flex: 1 1 0;
`;

const ProfileWidgetName = styled('a')`
  color: #115c87;
  font-size: 18px;
  text-decoration: none;
`;

const ProfileWidgetPosition = styled('div')`
  font-size: 13px;
`;

const ProfileWidget: FunctionComponent<ProfileWidgetProps> = ({
  name,
  info,
  avatar,
  link,
  children,
  ...otherProps
}) => {
  return (
    <ProfileWidgetContainer {...otherProps}>
      <ProfileWidgetAvatar src={avatar} title={name} link={link} />
      <ProfileWidgetInfo>
        <ProfileWidgetName href={link}>{name}</ProfileWidgetName>

        <ProfileWidgetPosition>{info}</ProfileWidgetPosition>
      </ProfileWidgetInfo>

      {children}
    </ProfileWidgetContainer>
  );
};

export default ProfileWidget;

interface ProfileWidgetProps {
  name: string;
  info: string;
  avatar: string;
  link: string;
}
