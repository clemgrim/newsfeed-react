import styled from '../../theme';

const Container = styled('div')`
  max-width: 980px;
  padding: 0 15px;
  margin: 0 auto;
`;

/** @component */
export default Container;
