import React, { FunctionComponent } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { Badge } from '../UI';
import styled from '../../theme';

const Container = styled('div')`
  position: relative;
  font-size: 25px;
`;

const StyledBadge = styled(Badge)`
  position: absolute;
  top: -5px;
  right: -15px;
  font-size: 10px;
`;

const AppHeaderCounter: FunctionComponent<AppHeaderCounterProps> = ({
  count,
  icon
}) => {
  return (
    <Container>
      <FontAwesomeIcon icon={icon} />
      {count > 0 && (
        <StyledBadge theming="primary">
          {count > 100 ? '99+' : count}
        </StyledBadge>
      )}
    </Container>
  );
};

export default AppHeaderCounter;

interface AppHeaderCounterProps {
  count: number;
  icon: IconProp;
}
