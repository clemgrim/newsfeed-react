import React, { useState, FunctionComponent } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faUserPlus,
  faEnvelope,
  faBell,
  faCogs,
  faUser,
  faUnlock
} from '@fortawesome/free-solid-svg-icons';
import { Avatar, DropdownMenu } from '../UI';
import AppHeaderCounter from './AppHeaderCounter';
import styled from '../../theme';

const HeaderContainer = styled('header')`
  width: 100%;
  position: relative;
  z-index: 200;
  box-shadow: 0 0 15px rgba(0, 0, 0, 0.15);
`;

const Navbar = styled('nav')`
  background-color: #fff;
  padding: 0 0 0 15px;
  display: flex;
  justify-content: space-between;
  height: 60px;
`;

const Menu = styled('ul')`
  padding: 0;
  margin: 0;
  list-style: none;
  display: flex;
  color: #526b7a;
  font-size: 15px;
`;

const MenuItem = styled('li')`
  position: relative;
  display: flex;
`;

const MenuLink = styled(NavLink)`
  padding: 0 15px;
  text-decoration: none;
  display: flex;
  align-items: center;
  color: inherit;

  &:hover,
  &.active {
    color: ${props => props.theme.colors.primary};
  }
`;

const Logo = styled(Link)`
  display: flex;
  border-right: 1px solid #e4e4e4;
  align-items: center;
  padding-right: 10px;
`;

const DropdownItem = styled(Link)`
  color: inherit;
  text-decoration: none;
`;

const AppHeader: FunctionComponent<AppHeaderProps> = ({
  messagesCount = 0,
  notificationsCount = 0,
  pendingConnectionsCount = 0
}) => {
  const [isDropDownOpen, setIsDropDownOpen] = useState(false);

  return (
    <HeaderContainer>
      <Navbar>
        <Logo to="/" target="_self">
          <img
            src="https://www.hosco.com/images/home/hosco-blue@2x.png"
            alt="Hosco"
            width="105"
          />
        </Logo>
        <Menu>
          <MenuItem>
            <MenuLink to="/">Accueil</MenuLink>
          </MenuItem>
          <MenuItem>
            <MenuLink to="https://www.hosco.com/en/advice">Advice</MenuLink>
          </MenuItem>
          <MenuItem>
            <MenuLink to="https://www.hosco.com/en/directory/jobs">
              Offers
            </MenuLink>
          </MenuItem>
          <MenuItem>
            <MenuLink to="https://www.hosco.com/en/directory/companies">
              Companies
            </MenuLink>
          </MenuItem>
          <MenuItem>
            <MenuLink to="https://www.hosco.com/en/directory/schools">
              Schools
            </MenuLink>
          </MenuItem>
          <MenuItem>
            <MenuLink to="https://www.hosco.com/en/directory/members">
              Members
            </MenuLink>
          </MenuItem>
          <MenuItem>
            <MenuLink to="https://www.hosco.com/en/inbox">
              <AppHeaderCounter icon={faEnvelope} count={messagesCount} />
            </MenuLink>
          </MenuItem>
          <MenuItem>
            <MenuLink to="https://www.hosco.com/en/notifications">
              <AppHeaderCounter icon={faBell} count={notificationsCount} />
            </MenuLink>
          </MenuItem>
          <MenuItem>
            <MenuLink to="https://www.hosco.com/en/pending-connections">
              <AppHeaderCounter
                icon={faUserPlus}
                count={pendingConnectionsCount}
              />
            </MenuLink>
          </MenuItem>
          <MenuItem
            onMouseEnter={() => setIsDropDownOpen(true)}
            onMouseLeave={() => setIsDropDownOpen(false)}
          >
            <MenuLink to="/">
              <Avatar src="https://www.hosco.com/image/logo/42332/40/40" />
            </MenuLink>
            <DropdownMenu isOpen={isDropDownOpen} placement="right">
              <DropdownItem
                to="https://www.hosco.com/en/member/clement-grimault"
                target="_self"
              >
                <FontAwesomeIcon icon={faUser} /> Profile
              </DropdownItem>
              <DropdownItem
                to="https://www.hosco.com/en/settings/account"
                target="_self"
              >
                <FontAwesomeIcon icon={faCogs} /> Account settings
              </DropdownItem>
              <DropdownItem
                to="https://www.hosco.com/en/settings/notifications"
                target="_self"
              >
                <FontAwesomeIcon icon={faBell} /> Notification settings
              </DropdownItem>
              <DropdownItem
                to="https://www.hosco.com/en/auth/logout"
                target="_self"
              >
                <FontAwesomeIcon icon={faUnlock} /> Logout
              </DropdownItem>
            </DropdownMenu>
          </MenuItem>
        </Menu>
      </Navbar>
    </HeaderContainer>
  );
};

export default AppHeader;

interface AppHeaderProps {
  messagesCount: number;
  notificationsCount: number;
  pendingConnectionsCount: number;
}
