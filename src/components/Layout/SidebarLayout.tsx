import React, { FunctionComponent, ReactNode } from 'react';
import styled from '../../theme';

import Container from './Container';

const LayoutContainer = styled(Container)`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  padding-top: 20px;
`;

const ContentContainer = styled('div')`
  flex: 1 1 0;
  min-width: 1px;
`;

const SidebarContainer = styled('aside')`
  flex: 0 0 340px;
  margin-left: 20px;
  position: relative;
`;

const SidebarLayout: FunctionComponent<SidebarProps> = ({
  renderSidebar,
  renderContent
}) => (
  <LayoutContainer>
    <ContentContainer>{renderContent()}</ContentContainer>
    <SidebarContainer>{renderSidebar()}</SidebarContainer>
  </LayoutContainer>
);

export default SidebarLayout;

interface SidebarProps {
  renderSidebar: () => ReactNode;
  renderContent: () => ReactNode;
}
