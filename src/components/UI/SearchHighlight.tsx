import React, { memo, FunctionComponent } from 'react';
import { smartMatch, MatchOptions } from '../../lib/util';

const SearchHighlight: FunctionComponent<SearchHighlightProps> = ({
  search,
  text,
  matchOptions
}) => {
  const match = smartMatch(search, text, matchOptions);

  if (!match || !match.groups || typeof match.index !== 'number') {
    return <>text</>;
  }

  const { groups, index } = match;

  const startIndex = groups.before ? index + groups.before.length : index;
  const matchedString = groups.match;
  const before = text.slice(0, startIndex);
  const highlight = text.slice(startIndex, startIndex + matchedString.length);
  const after = text.slice(startIndex + matchedString.length);

  return (
    <>
      {before}
      <strong>{highlight}</strong>
      {after}
    </>
  );
};

export default memo(SearchHighlight);

interface SearchHighlightProps {
  search: string;
  text: string;
  matchOptions?: MatchOptions;
}
