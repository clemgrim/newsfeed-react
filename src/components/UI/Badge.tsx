import styled, { AppTheme } from '../../theme';

const themes = {
  primary: (theme: AppTheme) => theme.colors.primary,
  secondary: (theme: AppTheme) => theme.colors.secondary,
  default: () => '#333'
};

const Badge = styled('span')<BadgeProps>`
  display: inline-block;
  border-radius: 6px;
  padding: 4px 6px;
  color: #fff;
  font-weight: bold;
  background-color: ${props => themes[props.theming](props.theme)};
`;

/** @component */
export default Badge;

interface BadgeProps {
  theming: keyof typeof themes;
}
