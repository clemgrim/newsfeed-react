import React, { memo, FunctionComponent } from 'react';
import { css } from '@emotion/core';

import styled from '../../theme';

const avatarSizes = {
  xs: '40px',
  md: '80px',
  lg: '200px'
};

const shadowStyle = (props: { rounded: boolean }) => css`
  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    box-shadow: inset 0 0 2px 0 rgba(0, 0, 0, 0.3);
    border-radius: ${props.rounded ? '50%' : ''};
  }
`;

const AvatarContainer = styled('div')<AvatarContainerProps>`
  position: relative;
  width: ${props => avatarSizes[props.size]};
  height: ${props => avatarSizes[props.size]};
  ${props => (props.shadow ? shadowStyle : '')}
`;

const AvatarImage = styled('img')<AvatarImageProps>`
  border-radius: ${props => (props.rounded ? '50%' : '')};
  max-width: 100%;
  height: auto;
`;

const Avatar: FunctionComponent<AvatarProps> = ({
  src,
  title,
  link,
  size = 'xs' as AvatarSizes,
  rounded = false,
  shadow = false,
  ...props
}) => {
  const img = <AvatarImage src={src} alt={title} rounded={rounded} />;

  return (
    <AvatarContainer size={size} shadow={shadow} rounded={rounded} {...props}>
      {link ? <a href={link}>{img}</a> : img}
    </AvatarContainer>
  );
};

export default memo(Avatar);

type AvatarSizes = keyof typeof avatarSizes;

interface AvatarProps {
  readonly src: string;
  readonly title?: string;
  readonly link?: string;
  readonly size?: AvatarSizes;
  readonly rounded?: boolean;
  readonly shadow?: boolean;
}

type AvatarContainerProps = Required<
  Pick<AvatarProps, 'size' | 'rounded' | 'shadow'>
>;

type AvatarImageProps = Required<Pick<AvatarProps, 'rounded'>>;
