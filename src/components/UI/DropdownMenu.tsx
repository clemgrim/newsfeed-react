import React, { FunctionComponent } from 'react';
import styled from '../../theme';

const DropdownContainer = styled('ul')<DropdownContainerProps>`
  display: ${props => (props.isOpen ? '' : 'none')};
  position: absolute;
  top: 100%;
  right: ${props => (props.placement === 'right' ? 0 : '')};
  left: ${props => (props.placement === 'left' ? 0 : '')};
  min-width: 160px;
  border: 1px solid rgba(0, 0, 0, 0.15);
  border-top: 0;
  background: #fff;
  padding: 5px 0;
  margin: 0;
  list-style: none;
`;

const DropdownMenuItem = styled('li')<DropdownMenuItemProps>`
  padding: 2px 10px;
  color: #526b7a;
  text-decoration: none;
  display: block;
  white-space: nowrap;
  cursor: pointer;
  background-color: ${props => (props.active ? '#f8f9fa' : '')};

  &:hover {
    background-color: #f8f9fa;
  }
`;

const DropdownMenu: FunctionComponent<DropdownMenuProps> = ({
  children,
  isOpen = false,
  activeIndex = -1,
  placement = 'left' as placement
}) => {
  return (
    <DropdownContainer isOpen={isOpen} placement={placement}>
      {React.Children.map(children, (item, idx) => (
        <DropdownMenuItem active={activeIndex === idx}>{item}</DropdownMenuItem>
      ))}
    </DropdownContainer>
  );
};

export default DropdownMenu;

type placement = 'right' | 'left';

interface DropdownMenuProps {
  readonly isOpen?: boolean;
  readonly placement?: placement;
  readonly activeIndex?: number;
}

interface DropdownMenuItemProps {
  readonly active: boolean;
}

type DropdownContainerProps = Required<
  Pick<DropdownMenuProps, 'isOpen' | 'placement'>
>;
