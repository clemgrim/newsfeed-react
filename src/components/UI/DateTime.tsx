import React, { useState, useEffect, memo, FunctionComponent } from 'react';
import { format, distanceInWordsStrict } from 'date-fns';

const DateTime: FunctionComponent<DateTimeProps> = ({
  date,
  refresh = true,
  absolute = false
}) => {
  const [now, setNow] = useState(new Date());

  useEffect(
    () => {
      if (refresh && !absolute) {
        const timer = setInterval(
          () => setNow(new Date()),
          getTimeToUpdate(date) * 1000
        );

        return () => clearInterval(timer);
      }
    },
    [date, refresh, absolute]
  );

  const datetime = format(date, 'YYYY-MM-DDTHH:mm:ssZ');
  const fullDate = format(date, 'dddd MMM Do YYYY, HH:mm');
  const dateObj = new Date(date);
  let humanDate;

  if (absolute) {
    humanDate =
      dateObj.getFullYear() === now.getFullYear()
        ? format(dateObj, 'MMMM DD')
        : format(dateObj, 'MMMM DD, YYYY');
  } else if (Date.now() - dateObj.getTime() < 1500) {
    humanDate = 'just now';
  } else {
    humanDate = distanceInWordsStrict(now, dateObj, {
      addSuffix: true
    });
  }

  return (
    <time title={fullDate} dateTime={datetime}>
      {humanDate}
    </time>
  );
};

const getTimeToUpdate = (value: Date | number | string) => {
  const date = new Date(value);
  const now = new Date();
  const timeFromNow = Math.abs(now.getTime() - date.getTime()) / 1000;

  // less than a minute
  if (timeFromNow < 60) {
    return 5;
  }

  // less than an hour
  if (timeFromNow < 60 * 60) {
    return 30;
  }

  // less then a day
  if (timeFromNow < 60 * 60 * 24) {
    return 60 * 5;
  }

  // update every hour
  return 60 * 60;
};

export default memo(DateTime);

interface DateTimeProps {
  readonly date: Date | number | string;
  readonly refresh?: boolean;
  readonly absolute?: boolean;
}
