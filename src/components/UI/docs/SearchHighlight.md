```jsx 
<SearchHighlight text="hé! Lorem ipsum dolor sit amet" search="ipsum" />
```

### Dynamic search
```jsx
const TextBox = require('../../Form/TextBox').default;

<>
  <TextBox value={ state.value } onChange={ e => setState({ value: e.currentTarget.value }) }/>
  <br /><br />
  <SearchHighlight text="hé! 🇫🇷 Lorem ipsum dolor sit amet" search={ state.value } />
</>
```

### Custom options
```jsx 
<SearchHighlight text="Lorem ipsum" search="rem" matchOptions={{ leading: false }} />
```
