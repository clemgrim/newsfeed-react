### Sizes
```jsx
<div className="inline-list">
  <Avatar src="https://placehold.it/200x200" title="My profile" size="xs" />
  <Avatar src="https://placehold.it/200x200" title="My profile" size="md" />
  <Avatar src="https://placehold.it/200x200" title="My profile" size="lg" />
</div>
````

### Rounded
```jsx
<div className="inline-list">
  <Avatar rounded src="https://placehold.it/200x200" title="My profile" size="xs" />
  <Avatar rounded src="https://placehold.it/200x200" title="My profile" size="md" />
  <Avatar rounded src="https://placehold.it/200x200" title="My profile" size="lg" />
</div>
````

### Shadow
```jsx
<div className="inline-list">
  <Avatar shadow src="https://placehold.it/200x200" title="My profile" size="xs" />
  <Avatar shadow src="https://placehold.it/200x200" title="My profile" size="md" />
  <Avatar shadow src="https://placehold.it/200x200" title="My profile" size="lg" />
  <Avatar shadow rounded src="https://placehold.it/200x200" title="My profile" size="xs" />
  <Avatar shadow rounded src="https://placehold.it/200x200" title="My profile" size="md" />
  <Avatar shadow rounded src="https://placehold.it/200x200" title="My profile" size="lg" />
</div>
````
