### Dates in the past
```jsx
const now = new Date().getTime();
const dates = {
  now: now,
  fewSeconds: now - 5 * 1000,
  oneMinute: now - 60 * 1000,
  fewMinutes: now - 5 * 60 * 1000,
  oneHour: now - 60 * 60 * 1000,
  fewHours: now - 13 * 60 * 60 * 1000,
  oneDay: now - 25 * 60 * 60 * 1000,
  fewDays: now - 75 * 60 * 60 * 1000,
  fewMonths: now - 4 * 30 * 24 * 60 * 60 * 1000,
  oneYear: now - 365 * 24 * 60 * 60 * 1000,
  fewYears: now - 5 * 365 * 24 * 60 * 60 * 1000,
};

<>
  <button onClick={() => setState({ absolute: !state.absolute }) }>Toggle absolute</button>
  
  { Object.entries(dates).map(([name, date]) => (
    <p key={ name }>
      <strong>{name}</strong> <DateTime date={date} absolute={ state.absolute } />
    </p>
  )) }
</>
```

### Dates in the future
```jsx
const now = new Date().getTime();
const dates = {
  fewSeconds: now + 5 * 1000,
  oneMinute: now + 60 * 1000,
  fewMinutes: now + 5 * 60 * 1000,
  oneHour: now + 60 * 60 * 1000,
  fewHours: now + 13 * 60 * 60 * 1000,
  oneDay: now + 25 * 60 * 60 * 1000,
  fewDays: now + 75 * 60 * 60 * 1000,
  fewMonths: now + 4 * 30 * 24 * 60 * 60 * 1000,
  oneYear: now + 365 * 24 * 60 * 60 * 1000,
  fewYears: now + 5 * 365 * 24 * 60 * 60 * 1000,
};

<>
  <button onClick={() => setState({ absolute: !state.absolute }) }>Toggle absolute</button>
  
  { Object.entries(dates).map(([name, date]) => (
    <p key={ name }>
      <strong>{name}</strong> <DateTime date={date} absolute={ state.absolute } />
    </p>
  )) }
</>
```

### Without refresh
```jsx
const now = new Date().getTime();
const date = now + 5 * 1000;

<DateTime date={date} refresh={ false } />
```
