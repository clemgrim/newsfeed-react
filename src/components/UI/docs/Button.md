### Button sizes

```jsx
<div className="inline-list">
  <Button size="xs">Button XS</Button>
  <Button size="sm">Button SM</Button>
  <Button size="md">Button MD</Button>
  <Button size="lg">Button LG</Button>
</div>
```

### Rounded buttons
```jsx
<div className="inline-list">
  <Button size="xs" rounded>Button XS</Button>
  <Button size="sm" rounded>Button SM</Button>
  <Button size="md" rounded>Button MD</Button>
  <Button size="lg" rounded>Button LG</Button>
</div>
```

### Themes
```jsx
<div className="inline-list">
  <Button theming="primary">Primary</Button>
  <Button theming="secondary">Secondary</Button>
  <Button theming="default">Default</Button>
  <Button rounded theming="primary">Primary</Button>
  <Button rounded theming="secondary">Secondary</Button>
    <Button rounded theming="default">Default</Button>
</div>
```

### Outline
```jsx
<div className="inline-list">
  <Button outlined theming="primary">Primary</Button>
  <Button outlined theming="secondary">Secondary</Button>
  <Button outlined theming="default">Default</Button>
  <Button rounded outlined theming="primary">Primary</Button>
  <Button rounded outlined theming="secondary">Secondary</Button>
  <Button rounded outlined theming="default">Default</Button>
</div>
```

### State
#### Disabled
```jsx
<div className="inline-list">
  <Button disabled theming="primary">Primary</Button>
  <Button disabled theming="secondary">Secondary</Button>
  <Button disabled theming="default">Default</Button>
  <Button outlined disabled theming="primary">Primary</Button>
  <Button outlined disabled theming="secondary">Secondary</Button>
  <Button outlined disabled theming="default">Default</Button>
</div>
```

#### Active
```jsx
<div className="inline-list">
  <Button active theming="primary">Primary</Button>
  <Button active theming="secondary">Secondary</Button>
  <Button active theming="default">Default</Button>
  <Button active outlined theming="primary">Primary</Button>
  <Button active outlined theming="secondary">Secondary</Button>
  <Button active outlined theming="default">Default</Button>
</div>
```

#### Loading
```jsx
<div className="inline-list">
  <Button loading theming="primary">Primary</Button>
  <Button loading theming="secondary">Secondary</Button>
  <Button loading theming="default">Default</Button>
  <Button loading outlined theming="primary">Primary</Button>
  <Button loading outlined theming="secondary">Secondary</Button>
  <Button loading outlined theming="default">Default</Button>
</div>
```
