import React, { memo, FunctionComponent } from 'react';
import { css } from '@emotion/core';

import styled, { AppTheme, WithTheme } from '../../theme';

const sizes = {
  xs: '4px 10px',
  sm: '8px 14px',
  md: '12px 18px',
  lg: '16px 22px'
};

const themes = {
  primary: (theme: AppTheme) => ({
    bg: theme.colors.primary,
    active: theme.colors.secondary,
    hover: theme.colors.secondary,
    color: '#fff'
  }),
  secondary: (theme: AppTheme) => ({
    bg: theme.colors.secondary,
    color: '#fff',
    active: theme.colors.secondary,
    hover: theme.colors.secondary
  }),
  default: () => ({
    bg: '#333',
    color: '#fff',
    active: '#000',
    hover: '#000'
  })
};

const disabledStyle = css`
  opacity: 0.75;
  cursor: not-allowed;
`;

const activeStyle = (props: WithTheme<ButtonProps>) => css`
  background-color: ${themes[props.theming as ButtonTheme](props.theme).active};
  color: ${props.outlined
    ? themes[props.theming as ButtonTheme](props.theme).color
    : ''};
  border-color: ${themes[props.theming as ButtonTheme](props.theme).active};
`;

const loadingStyle = css`
  cursor: wait !important;
`;

const StyledButton = styled('button')<ButtonProps>`
  font-size: 14px;
  box-shadow: none;
  cursor: pointer;
  border: 2px solid
    ${props => themes[props.theming as ButtonTheme](props.theme).bg};
  padding: ${props => sizes[props.size as ButtonSize]};
  border-radius: ${props => (props.rounded ? '1em' : 0)};
  background-color: ${({ outlined, theming, theme }) =>
    outlined ? 'transparent' : themes[theming as ButtonTheme](theme).bg};
  color: ${({ theming, theme, outlined }) =>
    themes[theming as ButtonTheme](theme)[outlined ? 'bg' : 'color']};

  &:disabled {
    ${disabledStyle};
  }

  &:hover {
    background-color: ${({ disabled, theming, theme, outlined }) =>
      !disabled &&
      themes[theming as ButtonTheme](theme)[outlined ? 'bg' : 'hover']};
    color: ${({ outlined, disabled, theming, theme }) =>
      outlined && !disabled ? themes[theming as ButtonTheme](theme).color : ''};
    border-color: ${({ outlined, disabled, theming, theme }) =>
      !disabled &&
      themes[theming as ButtonTheme](theme)[outlined ? 'bg' : 'hover']};
  }

  ${props => props.active && activeStyle};
  ${props => props.loading && loadingStyle};
`;

const Button: FunctionComponent<ButtonProps & NativeProps> = props => {
  const { children, loading } = props;

  return (
    <StyledButton {...props} disabled={props.disabled || props.loading}>
      {loading ? 'Loading...' : children}
    </StyledButton>
  );
};

Button.defaultProps = {
  theming: 'default',
  size: 'sm',
  outlined: false,
  rounded: false,
  active: false,
  loading: false,
  disabled: false
};

export default memo(Button);

type ButtonSize = keyof typeof sizes;

type ButtonTheme = keyof typeof themes;

interface ButtonProps {
  theming?: ButtonTheme;
  loading?: boolean;
  disabled?: boolean;
  active?: boolean;
  outlined?: boolean;
  size?: ButtonSize;
  rounded?: boolean;
}

type NativeProps = JSX.IntrinsicElements['button'];
