import React, {
  useState,
  useRef,
  useEffect,
  FunctionComponent,
  KeyboardEvent,
  ReactNode
} from 'react';
import styled from '@emotion/styled';
import { DropdownMenu, SearchHighlight } from '../UI';
import { debounce } from 'lodash-es';
import { useSlicedArray } from '../../lib/hooks';
import { smartMatch } from '../../lib/util';
import TextBox from './TextBox';

const SelectBoxContainer = styled('div')`
  display: table;
  position: relative;
`;

const defaultRenderSuggestion: RenderSuggestion<string> = (
  item,
  label,
  search
) => <SearchHighlight text={label} search={search} />;
const defaultFilterSuggestion: FilterSuggestion<string> = (item, input) =>
  smartMatch(input, item) !== null;
const defaultGetSuggestionLabel: GetSuggestionLabel<string> = item => item;

const SelectBox: SelectBoxOf<any> = ({
  suggestions,
  getSuggestionLabel = defaultGetSuggestionLabel,
  renderSuggestion = defaultRenderSuggestion,
  filterSuggestion = defaultFilterSuggestion,
  limit = 8,
  placeholder = '',
  onChange,
  value,
  disabled = false,
  debounce: debounceDelay = 0,
  minLength = 2
}) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [showSuggestions, setShowSuggestions] = useState(false);
  const [filtered, setFiltered] = useSlicedArray<string>([], limit);
  const [isLoading, setIsLoading] = useState(false);

  // We use a ref to disable re-rendering
  const inputRef = useRef<HTMLInputElement>(null);

  // Force the caret position
  useEffect(() => {
    if (inputRef.current) {
      const { length } = getInputValue();
      inputRef.current.setSelectionRange(length, length);
    }
  });

  const getInputValue = () => (inputRef.current ? inputRef.current.value : '');

  const onInputChange = debounce(() => {
    let userInput = getInputValue();

    if (userInput.length < minLength) {
      setShowSuggestions(false);
      return;
    }

    setActiveIndex(0);
    setShowSuggestions(true);

    if (Array.isArray(suggestions)) {
      if (userInput.length === 0) {
        setFiltered(suggestions);
      } else {
        setFiltered(
          suggestions.filter(item =>
            filterSuggestion(getSuggestionLabel(item), userInput, item)
          )
        );
      }
    } else {
      setIsLoading(true);

      suggestions(userInput).then(results => {
        setFiltered(results);
        setIsLoading(false);
      });
    }
  }, debounceDelay);

  const commitValue = (item: any) => {
    inputRef.current!.value = getSuggestionLabel(item);
    setShowSuggestions(false);
    setActiveIndex(0);

    onChange(item);
  };

  const onKeyDown = (e: KeyboardEvent<any>) => {
    switch (e.key) {
      case 'Enter':
        commitValue(filtered[activeIndex]);
        break;

      case 'ArrowUp':
        setActiveIndex(activeIndex > 0 ? activeIndex - 1 : 0);
        break;

      case 'ArrowDown':
        setActiveIndex(
          activeIndex < filtered.length - 1
            ? activeIndex + 1
            : filtered.length - 1
        );
        break;
    }
  };

  // We have to delay a bit the dropdown closing, as the blur event is called before the dropdown click
  const onBlur = () => setTimeout(() => setShowSuggestions(false), 200);

  const renderSuggestionList = (item: any, index: number) => {
    const label = getSuggestionLabel(item);

    return (
      <div key={label} onClick={() => commitValue(item)}>
        {renderSuggestion(
          item,
          label,
          getInputValue(),
          index,
          index === activeIndex
        )}
      </div>
    );
  };

  return (
    <SelectBoxContainer>
      <TextBox
        type="text"
        onChange={onInputChange}
        onKeyDown={onKeyDown}
        placeholder={placeholder}
        disabled={disabled}
        ref={inputRef}
        defaultValue={value ? getSuggestionLabel(value) : ''}
        onFocus={onInputChange}
        onBlur={onBlur}
      />
      <DropdownMenu isOpen={showSuggestions} activeIndex={activeIndex}>
        {(() => {
          if (isLoading) {
            return <em>Loading...</em>;
          }

          return filtered.length ? (
            filtered.map(renderSuggestionList)
          ) : (
            <em>No suggestions, you're on your own!</em>
          );
        })()}
      </DropdownMenu>
    </SelectBoxContainer>
  );
};

export default SelectBox;

export type SelectBoxOf<T> = FunctionComponent<SelectBoxProps<T>>;

interface SelectBoxProps<T> {
  suggestions: Suggestions<T>;
  getSuggestionLabel: GetSuggestionLabel<T>;
  renderSuggestion: RenderSuggestion<T>;
  filterSuggestion: FilterSuggestion<T>;
  limit: number;
  placeholder: string;
  onChange: Function;
  value: T;
  disabled: boolean;
  debounce: number;
  minLength: number;
}

type SuggestionAutocomplete<T> = (input: string) => Promise<T[]>;
type Suggestions<T> = T[] | SuggestionAutocomplete<T>;
type GetSuggestionLabel<T> = (item: T) => string;
type RenderSuggestion<T> = (
  item: T,
  label: string,
  search: string,
  index: number,
  isActive: boolean
) => ReactNode;
type FilterSuggestion<T> = (label: string, input: string, item: T) => boolean;
