import React, { forwardRef } from 'react';
import styled from '@emotion/styled';

const StyledInput = styled('input')`
  border: 1px solid #ccc;
  padding: 4px 8px;
  background-color: #fff;
  font-size: 14px;
  box-shadow: none;
`;

const StyledTextarea = StyledInput.withComponent('textarea');

const TextBox = forwardRef<any, TextBoxProps>(
  ({ type = 'text', ...props }, ref) => {
    if (type === 'textarea') {
      return <StyledTextarea {...props} ref={ref} />;
    }

    return <StyledInput type={type} {...props} ref={ref} />;
  }
);

export default TextBox;

type TextBoxProps = JSX.IntrinsicElements['input'] &
  JSX.IntrinsicElements['textarea'];
