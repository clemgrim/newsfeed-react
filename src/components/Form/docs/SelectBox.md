### Array of strings

```jsx
<SelectBox
  suggestions={require('./suggestions').map(item => item.name)}
  placeholder="Type something"
  onChange={noop.default}
/>
```

### Array of objects

```jsx
<SelectBox
  suggestions={require('./suggestions')}
  placeholder="Type something"
  getSuggestionLabel={item => item.name}
  onChange={noop.default}
/>
```

### Show everything before typing

```jsx
<SelectBox
  suggestions={['Approved', 'Rejected', 'Pending']}
  placeholder="Type something"
  onChange={noop.default}
  minLength={0}
/>
```

### Default value

```jsx
const suggestions = require('./suggestions');

<SelectBox
  suggestions={suggestions}
  placeholder="Type something"
  getSuggestionLabel={item => item.name}
  getSuggestionLabel={item => item.name}
  value={suggestions[5]}
  onChange={noop.default}
/>;
```

### Disabled

```jsx
<SelectBox
  suggestions={require('./suggestions')}
  placeholder="Type something"
  getSuggestionLabel={item => item.name}
  onChange={noop.default}
  disabled
/>
```

### Custom matching/render

```jsx
const SearchHighlight = require('../../UI').SearchHighlight;
const smartMatch = require('../../../lib/util/search').smartMatch;

<SelectBox
  suggestions={require('./suggestions')}
  placeholder="Type something"
  getSuggestionLabel={item => item.name}
  filterSuggestion={(name, input) =>
    smartMatch(input, name, { leading: false }) !== null
  }
  renderSuggestion={({ flag }, text, search) => (
    <>
      {flag}{' '}
      <SearchHighlight
        {...{ text, search }}
        matchOptions={{ leading: false }}
      />
    </>
  )}
  onChange={noop.default}
/>;
```

### Async with debounce

```jsx
const smartMatch = require('../../../lib/util/search').smartMatch;
const loadSuggestions = input => {
  return new Promise(resolve => {
    setTimeout(() => {
      const results = require('./suggestions').filter(
        item => smartMatch(input, item.name) !== null
      );
      resolve(results);
    }, 1500);
  });
};

<SelectBox
  suggestions={loadSuggestions}
  placeholder="Type something"
  getSuggestionLabel={item => item.name}
  onChange={noop.default}
  debounce={400}
/>;
```
