### Simple text input
```jsx 
<TextBox placeholder="First Name" />
```
   
### Textarea
```jsx
<TextBox placeholder="First Name" type="textarea" />
```
