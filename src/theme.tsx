import styled, {
  CreateStyled,
  WithTheme as StyledWithTheme
} from '@emotion/styled';

export const theme = {
  colors: {
    primary: '#1898c2',
    secondary: '#115c87',
    greyLight: '#f7f7f7',
    greyMedium: '#ccc',
    greyDark: '#999',
    blackLight: '#666',
    blackMedium: '#333',
    blackDark: '#101010',
    green: '#00D563',
    yellow: '#ffc300',
    orange: '#FF8A00',
    redLight: '#FF3E62',
    redMedium: '#D41638',
    redDark: '#843534'
  },
  borders: {
    top: '#E4E4E4',
    right: '#d5d5d5'
  }
};

export type AppTheme = typeof theme;

export type WithTheme<Props> = StyledWithTheme<Props, AppTheme>;

export default styled as CreateStyled<AppTheme>;
