import { createAction, ActionsUnion, AsyncAction } from '../helpers';
import { News } from '../models';

export const FETCH_NEWS_REQUEST = '@news/request';
export const FETCH_NEWS_SUCCESS = '@news/success';
export const FETCH_NEWS_FAILURE = '@news/failure';

export const NewsActions = {
  newsRequested: () => createAction(FETCH_NEWS_REQUEST),
  newsError: (error: Error) => createAction(FETCH_NEWS_FAILURE, error),
  newsLoaded: (news: News) => createAction(FETCH_NEWS_SUCCESS, news)
};

export type NewsActions = ActionsUnion<typeof NewsActions>;

export const fetchNews: AsyncAction = (id: number) => async dispatch => {
  dispatch(NewsActions.newsRequested());

  try {
    const news = await import('../data/newsfeed');
    const found = news.default.find(n => n.id === id);

    if (!found) {
      throw new Error('News not found');
    }

    dispatch(NewsActions.newsLoaded(found));
  } catch (e) {
    dispatch(NewsActions.newsError(e));
  }
};
