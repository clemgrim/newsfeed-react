import {
  FETCH_NEWS_REQUEST,
  FETCH_NEWS_SUCCESS,
  FETCH_NEWS_FAILURE,
  NewsActions
} from './actions';
import { Reducer } from 'redux';
import { News } from '../models';

const initialState = {
  item: undefined,
  error: undefined,
  isLoading: false
};

interface NewsState {
  item?: News;
  error?: Error;
  isLoading: boolean;
}

const newsReducer: Reducer<NewsState, NewsActions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case FETCH_NEWS_REQUEST:
      return { ...state, error: undefined, isLoading: true };
    case FETCH_NEWS_SUCCESS:
      return {
        ...state,
        error: undefined,
        isLoading: false,
        item: action.payload
      };
    case FETCH_NEWS_FAILURE:
      return { ...state, error: action.payload, isLoading: false };
    default:
      return state;
  }
};

export default newsReducer;
