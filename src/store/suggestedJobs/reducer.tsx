import {
  FETCH_SUGGESTED_JOBS_REQUEST,
  FETCH_SUGGESTED_JOBS_SUCCESS,
  FETCH_SUGGESTED_JOBS_FAILURE,
  SuggestedJobsActions
} from './actions';
import { Reducer } from 'redux';
import { SuggestedJob } from '../models';

const initialState = {
  items: [],
  error: undefined,
  isLoading: false
};

const jobsReducer: Reducer<JobState, SuggestedJobsActions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case FETCH_SUGGESTED_JOBS_REQUEST:
      return { ...state, error: undefined, isLoading: true };
    case FETCH_SUGGESTED_JOBS_SUCCESS:
      return {
        ...state,
        error: undefined,
        isLoading: false,
        items: action.payload
      };
    case FETCH_SUGGESTED_JOBS_FAILURE:
      return { ...state, error: action.payload, isLoading: false };
    default:
      return state;
  }
};

interface JobState {
  items: SuggestedJob[];
  error?: Error;
  isLoading: boolean;
}

export default jobsReducer;
