import { createAction, ActionsUnion, AsyncAction } from '../helpers';
import { SuggestedJob } from '../models';

export const FETCH_SUGGESTED_JOBS_REQUEST = '@suggestedJobs/request';
export const FETCH_SUGGESTED_JOBS_SUCCESS = '@suggestedJobs/success';
export const FETCH_SUGGESTED_JOBS_FAILURE = '@suggestedJobs/failure';

export const SuggestedJobsActions = {
  suggestedJobsRequested: () => createAction(FETCH_SUGGESTED_JOBS_REQUEST),
  suggestedJobsError: (error: Error) =>
    createAction(FETCH_SUGGESTED_JOBS_FAILURE, error),
  suggestedJobsLoaded: (jobs: SuggestedJob[]) =>
    createAction(FETCH_SUGGESTED_JOBS_SUCCESS, jobs)
};

export type SuggestedJobsActions = ActionsUnion<typeof SuggestedJobsActions>;

export const fetchSuggestedJobs: AsyncAction = () => async dispatch => {
  dispatch(SuggestedJobsActions.suggestedJobsRequested());

  try {
    const jobs = await import('../data/suggested-jobs');
    dispatch(SuggestedJobsActions.suggestedJobsLoaded(jobs.default));
  } catch (e) {
    dispatch(SuggestedJobsActions.suggestedJobsError(e));
  }
};
