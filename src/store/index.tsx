import {
  createStore,
  applyMiddleware,
  combineReducers,
  Middleware
} from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';

import newsfeed from './newsfeed';
import news from './news';
import identity from './identity';
import suggestedJobs from './suggestedJobs';

const appReducers = {
  newsfeed,
  news,
  suggestedJobs,
  identity
};

const reducers = combineReducers(appReducers);

let middlewares: Middleware[] = [thunkMiddleware];

if (process.env.NODE_ENV !== 'production') {
  middlewares.push(createLogger());
}

export default createStore(reducers, {}, applyMiddleware(...middlewares));

type Reducers = typeof appReducers;

export type AppState = { [K in keyof Reducers]: ReturnType<Reducers[K]> };
