import { createAction, ActionsUnion } from '../helpers';
import { Profile } from '../models';

export const IDENTITY_LOGOUT = '@identity/logout';
export const IDENTITY_LOGIN = '@identity/login';

export const IdentityActions = {
  doLogin: (user: Profile) => createAction(IDENTITY_LOGIN, user),
  doLogout: () => createAction(IDENTITY_LOGOUT)
};

export type IdentityActions = ActionsUnion<typeof IdentityActions>;
