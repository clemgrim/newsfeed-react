import { IDENTITY_LOGOUT, IDENTITY_LOGIN, IdentityActions } from './actions';
import { Reducer } from 'redux';
import { Profile } from '../models';

const initialState = {
  id: 99794,
  name: 'Clément Grimault',
  avatar: 'https://www.hosco.com/image/logo/42332/40/40',
  url: 'https://www.hosco.com/en/member/clement-grimault',
  title: 'Lead developer at Hosco',
  type: 'member'
};

const identityReducer: Reducer<Profile | null, IdentityActions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case IDENTITY_LOGOUT:
      return null;
    case IDENTITY_LOGIN:
      return action.payload;
    default:
      return state;
  }
};

export default identityReducer;
