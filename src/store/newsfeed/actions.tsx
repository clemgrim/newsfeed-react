import { News, Profile } from '../models';
import { createAction, ActionsUnion, AsyncAction } from '../helpers';

export const FETCH_NEWSFEED_SUCCESS = '@newsfeed/success';
export const FETCH_NEWSFEED_REQUEST = '@newsfeed/request';
export const FETCH_NEWSFEED_FAILURE = '@newsfeed/failure';
export const NEWSFEED_TOGGLE_LIKE = '@newsfeed/toggleLike';
export const NEWSFEED_SHARE = '@newsfeed/share';
export const NEWSFEED_ADD_NEWS = '@newsfeed/addNews';

export const NewsfeedActions = {
  newsfeedRequested: () => createAction(FETCH_NEWSFEED_REQUEST),
  newsfeedError: (error: Error) => createAction(FETCH_NEWSFEED_FAILURE, error),
  newsfeedLoaded: (news: News[]) => createAction(FETCH_NEWSFEED_SUCCESS, news),
  likeToggled: (id: number) => createAction(NEWSFEED_TOGGLE_LIKE, id),
  newsShared: (id: number) => createAction(NEWSFEED_SHARE, id),
  newsAdded: (news: News) => createAction(NEWSFEED_ADD_NEWS, news)
};

export type NewsfeedActions = ActionsUnion<typeof NewsfeedActions>;

export const fetchNewsfeed: AsyncAction = () => async dispatch => {
  dispatch(NewsfeedActions.newsfeedRequested());

  try {
    const news = await import('../data/newsfeed');
    dispatch(NewsfeedActions.newsfeedLoaded(news.default));
  } catch (e) {
    dispatch(NewsfeedActions.newsfeedError(e));
  }
};
export const shareNews: AsyncAction = (sharedId: number) => async dispatch => {
  dispatch(NewsfeedActions.newsShared(sharedId));
  dispatch(createNews('I shared this news'));
};
export const createNews: AsyncAction = (text: string) => async (
  dispatch,
  getState
) => {
  const { identity } = getState();
  const id = Date.now();

  dispatch(
    NewsfeedActions.newsAdded({
      comments: [],
      is_liked: false,
      share_count: 0,
      like_count: 0,
      comment_count: 0,
      url: 'https://www.hosco.com/fr/newsfeed/status/' + id,
      content: text,
      id: id,
      owner: identity as Profile,
      creation_date: new Date(),
      type: 'text',
      mentions: []
    })
  );
};
