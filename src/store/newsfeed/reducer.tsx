import { Reducer } from 'redux';
import { News } from '../models';
import {
  FETCH_NEWSFEED_REQUEST,
  FETCH_NEWSFEED_SUCCESS,
  FETCH_NEWSFEED_FAILURE,
  NEWSFEED_TOGGLE_LIKE,
  NEWSFEED_SHARE,
  NEWSFEED_ADD_NEWS,
  NewsfeedActions
} from './actions';

const initialState = {
  items: [],
  error: undefined,
  isLoading: false
};

interface NewsfeedState {
  items: News[];
  error?: Error;
  isLoading: boolean;
}
const newsfeedReducer: Reducer<NewsfeedState, NewsfeedActions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case FETCH_NEWSFEED_REQUEST:
      return { ...state, error: undefined, isLoading: true };
    case FETCH_NEWSFEED_SUCCESS:
      return {
        ...state,
        error: undefined,
        isLoading: false,
        items: action.payload
      };
    case FETCH_NEWSFEED_FAILURE:
      return { ...state, error: action.payload, isLoading: false };
    case NEWSFEED_TOGGLE_LIKE:
      const newsLiked = state.items.find(o => o.id === action.payload);

      if (!newsLiked) {
        return state;
      }

      const isLiked = newsLiked.is_liked;

      return {
        ...state,
        items: state.items.map(item =>
          item === newsLiked
            ? {
                ...item,
                is_liked: !isLiked,
                like_count: newsLiked.like_count + (isLiked ? -1 : 1)
              }
            : item
        )
      };
    case NEWSFEED_SHARE:
      const sharedNews = state.items.find(o => o.id === action.payload);

      return {
        ...state,
        items: state.items.map(item =>
          item === sharedNews
            ? {
                ...item,
                share_count: sharedNews.share_count + 1
              }
            : item
        )
      };
    case NEWSFEED_ADD_NEWS:
      return {
        ...state,
        items: [action.payload, ...state.items]
      };
    default:
      return state;
  }
};

export default newsfeedReducer;
