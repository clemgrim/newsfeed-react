export interface Profile {
  avatar: string;
  id: number;
  name: string;
  type: string;
  url?: string;
}
