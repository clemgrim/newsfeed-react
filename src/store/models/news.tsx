import { Profile } from './profile';

interface NewsBase {
  id: number;
  content: string;
  type: string;
  is_liked: boolean;
  share_count: number;
  like_count: number;
  comment_count: number;
  url: string;
  story?: string;
  comments: Comment[];
  owner: Profile;
  mentions: Mention[];
  creation_date: string | Date;
  update_date?: string | Date;
}

export interface PictureNews extends NewsBase {
  type: 'picture';
  thumbs: Picture[];
  files: Picture[];
}

export interface SharedNews extends NewsBase {
  type: 'share';
  parent: News;
}

export interface TextNews extends NewsBase {
  type: 'text';
}

export interface LinkNews extends NewsBase {
  type: 'link';
  link: Link;
}

export type News = SharedNews | PictureNews | TextNews | LinkNews;

export interface Link {
  type: 'website' | 'youtube' | 'viemo';
  id: number;
  host: string;
  url: string;
  creation_date: string;
  title?: string;
  picture?: string;
  description?: string;
  date?: string;
}

export interface Comment {
  id: number;
  content: string;
  mentions: Mention[];
  owner: Profile;
  creation_date: string;
  update_date?: string;
}

export interface Mention {
  profile: Profile;
  offset_start: number;
  length: number;
}

export interface Picture {
  path: string;
  width: number;
  height: number;
  type: string;
  extension: string;
  title?: string;
}
