export interface SuggestedJob {
  id: number;
  title: string;
  link: string;
  info: string;
  picture: string;
}
