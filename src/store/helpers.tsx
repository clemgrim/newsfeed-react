import {
  ActionCreator,
  Action,
  ActionCreatorsMapObject as ActionMap
} from 'redux';
import { ThunkAction } from 'redux-thunk';
import { AppState } from '.';

export function createAction<T extends string>(type: T): Action<T>;
export function createAction<T extends string, P>(
  type: T,
  payload: P
): ActionWithPayload<T, P>;
export function createAction<T extends string, P>(type: T, payload?: P) {
  return payload ? { type, payload } : { type };
}

export type ActionWithPayload<T, P> = Action<T> & { payload: P };

export type AsyncAction = ActionCreator<ThunkAction<void, AppState, any, any>>;

export type ActionsUnion<A extends ActionMap> = ReturnType<A[keyof A]>;
