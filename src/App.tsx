import React, { lazy, Suspense } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import styled from '@emotion/styled';
import { ThemeProvider } from 'emotion-theming';
import { Provider as StoreProvider } from 'react-redux';

import { theme } from './theme';
import store from './store';

import AppHeader from './components/Layout/AppHeader';

const Home = lazy(() =>
  import(/* webpackChunkName: "home-page" */ './pages/Home')
);
const News = lazy(() =>
  import(/* webpackChunkName: "news-page" */ './pages/News')
);
const NotFound = lazy(() =>
  import(/* webpackChunkName: "not-found-page" */ './pages/NotFound')
);

export default () => {
  return (
    <Router>
      <StoreProvider store={store}>
        <ThemeProvider theme={theme}>
          <AppContainer>
            <AppHeader
              messagesCount={0}
              notificationsCount={0}
              pendingConnectionsCount={0}
            />
            <Suspense fallback={<div>Loading...</div>}>
              <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/news/:id(\d+)" component={News} />
                <Route component={NotFound} />
              </Switch>
            </Suspense>
          </AppContainer>
        </ThemeProvider>
      </StoreProvider>
    </Router>
  );
};

const AppContainer = styled('div')`
  background-color: #efefef;
  min-height: 100vh;
`;
