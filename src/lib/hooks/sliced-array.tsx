import { useState } from 'react';

export default function<T>(
  initial: T[],
  limit: number
): [T[], (v: T[]) => void] {
  const [value, setValue] = useState(initial);
  const setter = (v: T[]) => setValue(v.slice(0, limit));

  return [value, setter];
}
