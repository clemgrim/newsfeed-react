import { deburr, escapeRegExp } from 'lodash-es';

export const createTestPattern = (
  search: string,
  options: MatchOptions = {}
) => {
  const {
    leading = true,
    ignoreAccents = true,
    caseInsensitive = true
  } = options;

  if (ignoreAccents) {
    search = deburr(search);
  }

  search = escapeRegExp(search);

  const pattern = leading
    ? `^(?<before>.*[ -()\\/[\\]])?(?<match>${search})`
    : `(?<match>${search})`;

  return RegExp(pattern, caseInsensitive ? 'i' : '');
};

export const smartMatch = (
  search: string,
  text: string,
  { ignoreAccents = true, ...opts }: MatchOptions = {}
) => {
  if (!text) {
    return null;
  }

  if (ignoreAccents) {
    text = deburr(text);
  }

  const pattern = createTestPattern(search, { ignoreAccents, ...opts });

  return text.match(pattern);
};

export interface MatchOptions {
  leading?: boolean;
  ignoreAccents?: boolean;
  caseInsensitive?: boolean;
}
