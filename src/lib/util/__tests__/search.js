import { createTestPattern } from '../search';

describe('test createTestPattern', () => {
  test('createTestPattern should return a regexp', () => {
    expect(createTestPattern()).toBeInstanceOf(RegExp);
  });
});

