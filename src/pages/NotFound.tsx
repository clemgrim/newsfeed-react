import React, { FunctionComponent } from 'react';
import { Helmet } from 'react-helmet';
import styled from '@emotion/styled';

const Title = styled('h1')`
  font-size: 30px;
  margin: 30px 0;
  font-weight: normal;
`;

const Container = styled('div')`
  background-color: #fff;
  overflow: hidden;
`;

const NotFoundPage: FunctionComponent = () => {
  return (
    <Container className="NotFoundPage">
      <Helmet>
        <title>Page not found</title>
      </Helmet>

      <Title>Page not found</Title>
    </Container>
  );
};

export default NotFoundPage;
