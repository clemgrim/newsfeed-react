import React, { FunctionComponent } from 'react';
import { Helmet } from 'react-helmet';

import NewsForm from '../containers/NewsForm';
import SidebarLayout from '../components/Layout/SidebarLayout';
import CurrentProfileWidget from '../containers/CurrentUserWidget';
import SuggestedJobsWidget from '../containers/SuggestedJobsWidget';
import NewsFeed from '../containers/NewsFeed';

const HomePage: FunctionComponent = () => {
  return (
    <>
      <Helmet>
        <title>Newsfeed</title>
      </Helmet>
      <SidebarLayout
        renderContent={() => (
          <>
            <NewsForm avatar="https://www.hosco.com/image/logo/42332/40/40" />
            <NewsFeed />
          </>
        )}
        renderSidebar={() => (
          <>
            <CurrentProfileWidget />
            <SuggestedJobsWidget />
          </>
        )}
      />
    </>
  );
};

export default HomePage;
