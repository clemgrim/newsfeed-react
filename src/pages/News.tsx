import React, { FunctionComponent } from 'react';
import { RouteComponentProps } from 'react-router';
import { Helmet } from 'react-helmet';

import SidebarLayout from '../components/Layout/SidebarLayout';
import CurrentProfileWidget from '../containers/CurrentUserWidget';
import SuggestedJobsWidget from '../containers/SuggestedJobsWidget';
import NewsDetails from '../containers/NewsDetails';

const NewsPage: FunctionComponent<NewsPageProps> = ({ match }) => {
  return (
    <>
      <Helmet>
        <title>News details</title>
      </Helmet>

      <SidebarLayout
        renderContent={() => (
          <>
            <NewsDetails newsId={+match.params.id} />
          </>
        )}
        renderSidebar={() => (
          <>
            <CurrentProfileWidget />
            <SuggestedJobsWidget />
          </>
        )}
      />
    </>
  );
};

export default NewsPage;

interface NewsPageProps extends RouteComponentProps<{ id: string }> {}
